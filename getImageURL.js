
var getImageURL = function( reference, PROJECTID, DATASET, CDN ) {

    var cdn = CDN || 'https://cdn.sanity.io/', // default
        dir = {
            images: 'images/',  // default
            files: 'files/'     // default
        },
        reference   = reference,
        projectId   = PROJECTID || projectId, // access global var projectId
        dataset     = DATASET   || dataset;   // access global var dataset

    if ( !reference )   console.error('Given reference is not valid.')
    if ( !dataset )     console.error('Please set variable dataset on global scope or pass it as parameter.')
    if ( !projectId )   console.error('Please set variable projectId on global scope or pass it as parameter.')

    if ( !projectId || !dataset ) return;

    reference = reference.replace('image-','').replace('-jpg','.jpg').replace('-png','.png').replace('-gif','.gif')
    return cdn + dir.images + projectId + '/' + dataset + '/' + reference;

}