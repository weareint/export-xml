var Promise = window.Promise;
if (!Promise) {
    Promise = JSZip.external.Promise;
}

function urlToPromise(url) {
    return new Promise(function(resolve, reject) {
        JSZipUtils.getBinaryContent(url, function (err, data) {
            if(err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

var getXML = function() {
    
    var projectId   = document.querySelector('#projectId').value,
    dataset     = document.querySelector('#dataset').value;
    
    var client = window.SanityClient({
        projectId: projectId,
        dataset: dataset
    })
    
    var query = `*[0...1000]`
    var documents = []

    var assetsFolder = 'Links';

    var zip = new JSZip();

    var output = '',
        outputToZip = true,
        outputToConsole = true;

    output += '<?xml version="1.0" encoding="utf-8"?>' + "\n"
    output += '<Root>' + "\n"
    
    client.fetch( query ).then( function( result ) {

        getDocuments( result ).forEach( function( document ) {
            var obj = {};
            documents.push(client.fetch( `*[_type == '${document}']` ).then( function( result ) {
                obj[ document ] = result // => item.documentname = { ... }
                output += objectToXML( obj )
            }))
        })
        
        Promise
        .all( documents )
        .then( () => { output += "\n\n</Root>" })
        .then( () => {
            

            // Convert images
            var regex = /<_type>image<\/_type>\n?.*<asset>\n?.*<_ref>.*<\/_ref>[\s\S]*?<\/asset>/gi, // finds the whole section that needs to be replaced
            match = output.match( regex );
            
            for ( var i = 0; i < match.length; i++ ) {

                if ( typeof getImageURL !== 'function' ) { 
                    console.error('Please provide a function getImageURL() to include images.');
                    break;
                }

                output = output.replace(match[i], function( item ) { 
                    
                    var isImageReference = item.match(/<_ref>image-.*<\/_ref>/);
                    if ( !isImageReference ) return item;

                    var indent = item.match(/.*?(?=<asset>)/)[0];
                    var reference = item.match(/<_ref>image[\s\S]*?<\/_ref>/)[0].replace('<_ref>','').replace('</_ref>','');
                    var imageURL = getImageURL( reference, projectId, dataset );
                    
                    var filename = imageURL.replace(/(https:\/\/).*\//g, "");
                    var image = '<image href="' + assetsFolder + '/' + filename + '" />'
                    
                    zip.file( assetsFolder + '/' + filename, urlToPromise( imageURL ), { binary:true });
                    
                    return image // => replace match[i] with image
                })
            }       

            /*
            // Clean up <_type> <_id> <_rev> etc.
            regex = / *<_.*>.*?<\/_.*>\n/gi;
            match = output.match( regex );
            for ( var i = 0; i < match.length; i++ ) {
                output = output.replace(match[i], '')
            }
            */

            if ( outputToZip ) {
                // Download
                zip.file( dataset+'.xml', output )
                zip.generateAsync({type:"blob"}).then(function callback(blob) {
                    download( blob, dataset + '.zip', 'application/zip');
                });
            } 
            if ( outputToConsole ) {
                console.log( output )
            }
            
        });
        
    })
    .catch(renderError)
    
    function getDocuments( result ) {
        var documents = []
        
        result.forEach( function( item ) {
            if ( documents.indexOf( item._type ) == '-1' && !item._type.includes( '.' ) ) documents.push( item._type )
        })
        
        return documents
    }
    
    function objectToXML( obj, i ) {
        var xml = '';
        var i = i || 0,
        indent = '';
        
        for( var j = 0; j < i; j++ ) {
            indent += '    ';
        }
        
        var k = 0
        for (var prop in obj) {
            
            if ( !obj.hasOwnProperty(prop) )  continue;
            if ( obj[prop] == undefined )     continue;

            if ( prop == '_createdAt'   || 
                 prop == '_updatedAt'   || 
                 prop == '_rev'         || 
                 prop == '_key'         ||
                 prop == 'style'
                ) continue;
            
            if ( Array.isArray(obj[prop]) ) {

                if (i == 0) xml += "\n" + "<_" + prop + ">";

                obj[prop].forEach(function( item, index ) {

                    var atts = '';
                    
                    atts += item._id ? ' id="'+item._id+'"':'';
                    atts += item._key ? ' key="'+item._key+'"':'';
                    atts += item._type ? ' type="'+item._type+'"':'';
                    atts += item._ref ? ' reference="'+item._ref+'"':'';
                    atts += item.style ? ' style="'+item.style+'"':'';

                    if ( i == 0 ) {

                        xml += "\n" + "<" + prop + atts +">";
                        xml += typeof item == "object" ? objectToXML( new Object( item ), i+1 ) + "\n" : escapeHtml(item)
                        xml += "</" + prop + ">";

                    } else {
                        
                        if ( (obj[prop][0] != obj[prop][1] && i != 0) && index == 0 ) xml += "\n" + "<" + prop + atts +">";
    
                        xml += typeof item == "object" ? objectToXML( new Object( item ), i+1 ) + "\n" : escapeHtml(item)
    
                        if ( (obj[prop][0] != obj[prop][1] && i != 0) && index == obj[prop].length-1 ) xml += "</" + prop + ">";
                    }

                })

                if (i == 0) xml += "\n" + "</_" + prop + ">" ;
            } else {
                xml += "\n" + "<" + prop + ">";
                xml += typeof obj[prop] == "object" ? objectToXML( new Object(obj[prop]), i+1 ) + "\n" : escapeHtml(obj[prop])
                xml += "</" + prop + ">" ;
            }
            
            k++
            
        }
        
        return xml;
    }
    
    function renderError(err) {
        var errorBox = document.createElement('pre')
        errorBox.innerHTML = err.message;
        errorBox.className = 'error'
        
        document.body.appendChild( errorBox )
    }
    
}

function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }
